<?php


class AdwordsAdvertisingSystem extends BaseAdvertisingSystem
{
    /**
     * AdwordsAdvertisingSystem Constructor.
     */
    public function __constructor()
    {
        $this->fileName = 'files/import_adwords.csv';
        $this->advertisingSystemId = 1;
    }

    /**
     * Load file and returns extracted data.
     *
     * @return array Data
     */
    public function loadFile(): array
    {
        $csvSettings = new CsvSettings(';', '"', 0);
        return Convertor::getArrayFromCsv($this->fileName, $csvSettings);
    }

    /**
     * Format data and array into required.
     *
     * @param array $data
     * @return array
     */
    public function getFormattedData(array $data): array {
        // TODO get needed data, format into base array which can be processed with foreach
        return $data;
    }
}