<?php

/**
 * Class Convertor
 */
class Convertor
{
    /**
     * Convert CSV to array.
     *
     * @param string $fileName Filename
     * @param CsvSettings $csvSettings CSV settings
     * @return array
     */
    public static function getArrayFromCsv(string $fileName, CsvSettings $csvSettings): array
    {
        $data = [];

        if (($handle = fopen($fileName, "r")) !== false) {

            while (($data = fgetcsv($handle, $csvSettings['length'], $csvSettings['delimiter'], $csvSettings['enclosure'])) !== false) {
                // TODO load csv file and return it as a array
            }
            fclose($handle);
        }

        return $data;
    }

    /**
     * Convert JSON to array.
     *
     * @param string $filename Filename
     * @return array
     */
    public static function getArrayFromJson(string $filename): array
    {
        // TODO load json file and return it as a array, something like this, depends on data
        $data = [];

        $fileContent = file_get_contents($filename);

//        if ($filename === null)
//            log

        return $data;
    }

    /**
     * Convert XML to array.
     *
     * @param string $filename Filename
     * @return array
     */
    public static function getArrayFromXml(string $filename): array
    {
        // TODO load xml file and return it as a array
        $data = [];

        $fileContent = file_get_contents($filename);

//        if ($filename === null)
//            log

        return $data;
    }
}