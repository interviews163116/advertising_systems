<?php


class AdvertisingSystemModel
{
    private string $datetime;
    private string $campaign;
    private int $impressionsNumber;
    private int $clicksNumber;
    private int $conversionsNumber;
    private float $price;
    private array $keywords;

    /**
     * @return string Datetime
     */
    public function getDatetime(): string
    {
        return $this->datetime;
    }

    /**
     * @param string $datetime Datetime
     */
    public function setDatetime(string $datetime): void
    {
        $this->datetime = $datetime;
    }

    /**
     * @return string Campaign
     */
    public function getCampaign(): string
    {
        return $this->campaign;
    }

    /**
     * @param string $campaign Campaign
     */
    public function setCampaign(string $campaign): void
    {
        $this->campaign = $campaign;
    }

    /**
     * @return int Impressions Number
     */
    public function getImpressionsNumber(): int
    {
        return $this->impressionsNumber;
    }

    /**
     * @param int $impressionsNumber Impressions Number
     */
    public function setImpressionsNumber(int $impressionsNumber): void
    {
        $this->impressionsNumber = $impressionsNumber;
    }

    /**
     * @return int Clicks Number
     */
    public function getClicksNumber(): int
    {
        return $this->clicksNumber;
    }

    /**
     * @param int $clicksNumber Clicks Number
     */
    public function setClicksNumber(int $clicksNumber): void
    {
        $this->clicksNumber = $clicksNumber;
    }

    /**
     * @return int Conversions Number
     */
    public function getConversionsNumber(): int
    {
        return $this->conversionsNumber;
    }

    /**
     * @param int $conversionsNumber Conversions Number
     */
    public function setConversionsNumber(int $conversionsNumber): void
    {
        $this->conversionsNumber = $conversionsNumber;
    }

    /**
     * @return float  Price
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price Price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return array
     */
    public function getKeywords(): array
    {
        return $this->keywords;
    }

    /**
     * @param array $keywords
     */
    public function setKeywords(array $keywords): void
    {
        $this->keywords = $keywords;
    }
}