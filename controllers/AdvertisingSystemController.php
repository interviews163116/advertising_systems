<?php

class AdvertisingSystemController extends BaseController
{
    private array $adSystems;

    /**
     * AdvertisingSystemController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->adSystems[] = new SklikAdvertisingSystem();
        $this->adSystems[] = new BingAdvertisingSystem();
        $this->adSystems[] = new AdwordsAdvertisingSystem();
        $this->adSystems[] = new YahooAdvertisingSystem();
    }

    /**
     * AJAX Action which inits ad data load.
     *
     * @return bool
     */
    public function actionLoadAdData(): bool
    {
        return $this->loadAdData();
    }

    /**
     * Method gradually processes all advertising systems.
     *
     * @return bool
     */
    private function loadAdData(): bool
    {
        $isOk = true;

        foreach ($this->adSystems as $adSystem) {
            if (!$adSystem->getProcessedAdData()) {
                $isOk = false;
            }
        }

        return $isOk;
    }
}